################################################################################
# Package: PanTauExample
################################################################################

# Declare the package name:
atlas_subdir( PanTauExample )

# Install files from the package:
atlas_install_joboptions( share/*.py )

